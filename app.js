const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

/*Executar no servidor ----------- NODE (NOME DO ARQUIVO)
Ctrl+C ----------- PARAR EXECUÇÃO NO SERVIDOR
Habilitar servidor*/

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/aluno',(req,res)=>{ 
    res.send("{message: Aluno encontrado!}");

});

//RECURSO DE REQUEST.QUERY
app.get('/aluno/filtros',(req,res)=>{
    let source = req.query;
    let ret = "Dados Solicitado: " + source.nome + " " + source.sobrenome;
    res.send("{message:"+ ret + "}");
});

//RECURSO DE REQUEST.PARAM
app.get('/aluno/pesquisa/:valor',(req,res)=>{
    console.log("Entrou");
    let dado = req.params.valor;
    let ret = "Dados Solicitado:" + dado;
    res.send("{message:"+ ret + "}");
});

//RECURSO DE REQUEST.BODY
app.post('/aluno',(req,res)=>{
    let dados = req.body;
    let ret = "\nDados Enviados:" + dados.nome;
    ret += "\nSobrenome: " + dados.sobrenome;
    ret += "\nIdade: " + dados.idade;
    res.send("{message:"+ ret +"}"
    );
});


